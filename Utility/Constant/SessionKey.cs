﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Constant
{
    public class SessionKey
    {
        public static readonly string UserID = "UserID";
        public static readonly string UserFullName = "UserFullName";
        public static readonly string ServiceProvider = "ServiceProvider";
    }
}
