﻿
namespace Utility.Constant
{
    public class PassengerType
    {
        public static readonly string ADULT = "ADLT";
        public static readonly string CHILDREN = "CTBL";
        public static readonly string INFANT = "INF";
    }
}
